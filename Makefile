default:
	@echo "The script is in 'source'"

private-update:
	@echo "updating cript"
	
	# update package dir
	@cp -r source/cript example.d COPY README NEWS package
	@cd package; gpg --sign --detach-sign cript
	
	# update web with new files, packages, etc
	@bash -c 'cd package; tar -zcf "../web/downloads/cript.tar.gz" *;cd ../web/downloads; cp cript.tar.gz cript-`../../package/cript -V`.tar.gz' # versions!
	
	# send to public directory and update feeds
	@cp -r web/* /me/public/projects/cript # copy project website to public
	@cd /me/public; bash update-website.sh #issues a website rebuild
	
	@echo "done"
