#!/bin/bash

<<COPYTEXT
    This script was written by Sulaiman A. Mustafa in 2014-01.


    I, the author of this work, hereby denounce the notion of Intellectual 
    Property (IP) in its entirety. This work is information. Information 
    can not be owned, in spite of those who dictate otherwise.

    Should my work be subject to any intellectual property laws, I reserve 
    all the arbitrary "rights" it defines.

    If you subscribe to the premise set forth thus far, you have my full 
    blessings to use and distribute my work as you see fit, but know that 
    doing so is deemed unlawful by IP laws as I DO NOT grant you such a 
    license. 

    Also note that other people may use your works under the same premise 
    as using this work in this manner constitutes your full agreement with 
    my position.
    
    If you, for any reason, wish or need to limit your natural rights by 
    using this software within the legal IP framework of some jurisdiction, 
    I hereby grant you a license to use this software under the terms of 
    the GPLv3 or later.



END OF
COPYTEXT


CRIPT_PACKAGE_VERSION="1.0.3"
[[ "$1" == "-V" ]] && {
    echo "$CRIPT_PACKAGE_VERSION"
    exit 0
}

COPY='<<NOTICE

    CRIPT PACKAGE '"$CRIPT_PACKAGE_VERSION"'
    

    For your rights to redistribution, incorporation, and other "Intellectual 
    Property" matters relating to this script, refer to the cript project:
    
        https://bitbucket.org/seininn/cript
    
    The compressed source code embedded in this script (SOURCE_TARBALL) is NOT 
    a part of the cript project and may be licensed under deferent terms.
    
END OF
NOTICE'

NEWLINE='
'

parse_args(){
<<DOC
    Defines an assosiative array, ARGS, and parses the functions arguments
    searching for any string starting with a '-' which it uses (without the
    '-') as a key and stores the subsequent arguement as its value.
    It also stores any non-option argument in a stack regardless of whether or
    not it is a value to some function.
    
    Example
        
        parse_args arg1 arg2 -option1 value1 -option2 arg3 arg4
        
    this will produce
        ARGS = ( [option1]=value1, [option2]=arg3, [5]=arg1, [4]=arg2, 
                 [3]=value1, [2]=arg3, [1]=arg4 )
    which should be defined in the global scope.
    
DOC

    declare -gA ARGS;
    while [[ "$@" ]]; do
        if [[ ${1:0:1} == "-" ]]; then 
            if [[ "$2" ]]; then
                ARGS["${1:1}"]="$2"
            else
                ARGS["${1:1}"]="-"
            fi
        else
            ARGS["5"]="${ARGS[4]}"
            ARGS["4"]="${ARGS[3]}"
            ARGS["3"]="${ARGS[2]}"
            ARGS["2"]="${ARGS[1]}"
            ARGS["1"]="$1"
        fi
        shift 1
    done
}
get_marked_section(){
<<DOC
    Takes a file name and a token TOKEN, and prints the section in the file
    delemented my the lines (without the perceeding whitespace):
        
        #MARKER:TOKEN:BEGIN
    
    and
    
        #MARKER:TOKEN:END
    
    Returns 0 on success and a non-zero value on failure:
    1: File not found
    2: Marker not found
DOC

    [[ -e "$1" ]] || return 1
    OIFS=$IFS
    IFS=':'
    BEGIN=($(grep -b "#MARKER:$2:BEGIN" "$1"))
    END=($(grep -b "#MARKER:$2:END" "$1"))
    
    [[ "$BEGIN" ]] || return 2
    [[ "$END" ]] || return 2
    
    ((LENGTH = END-BEGIN))
    
    dd bs=1 count=$LENGTH skip=$BEGIN if="$1" 2>/dev/null | tail -n +2
}

compress_source() {
<<DOC
    Takes in the pathname for the root directory of the c source tree, 
    compreses it, and encodes it in base64.
    
    Paramiters: Path name to source tree. (required).
    
    stdin: -
    stdout: -
    
    Returns: 0 on success and:
    1: generic error code
    2: not a directory
    3: tar failed
DOC

    [[ -d "$1" ]] || return 2
    cd "$1" > /dev/null
    tar -cz . | base64 || {
        cd - > /dev/null
        return 3
    }
    cd - > /dev/null
    return 0
}

USAGE="Usage
  $0  [-x -b SCRIPT-NAME]  SIGN-KEY-ID  PATH-TO-SOURCE-DIR  NAME"'

SIGN-KEY-ID is the id of the gpg key to sign the generated package with.
Users can verify the authenticity of the package with gpg by issuing the 
following command after importing the author"'"'"'s public key:

    gpg --verify NAME.sig NAME
    
PATH-TO-SOURCE-DIR is the path to the source code directory.

NAME is the name of the generated package as well as the name of the compiled
executable.

OPTIONS
    -x                      Bypasses the signing stage. Not recommended.
    -b SCRIPT-NAME          Change name of script to execute for the build 
                            script. Default is `build.sh'"'.

Written by Sulaiman A. Mustafa"

parse_args $*

[[ -d "${ARGS[2]}" ]] || {
    echo -n "No directory supplied. "
    [[ "${ARGS[2]}" ]] && echo -n "'${ARGS[2]}' is not a directory."
    echo "$NEWLINE$USAGE"
    exit 1
}

[[ "${ARGS[1]}" ]] || {
    echo "No name supplied."
    echo $USAGE
    exit 1
}


[[ "${ARGS[f]}" ]] || {
    [[ -e "${ARGS[1]}" ]] && {
        echo -n "File "'`'"${ARGS[1]}' exists. Overwrite? (y/N) "
        read ANSWER
        [[ "$ANSWER" != 'y' ]] && exit
    }
}

BUILD_SCRIPT_NAME="build.sh"
[[ "${ARGS[b]}" ]] && {
    echo "<i> build script name set to '${ARGS[b]}'."
    BUILD_SCRIPT_NAME="${ARGS[b]}"
}

FILE=${ARGS[1]}

echo "#!/bin/bash" > $FILE || {
    echo "<!> Could not write to "'`'$FILE"'"
    exit 1
}
echo "$COPY" >> $FILE
echo "SOURCE_TARBALL='" >> $FILE
compress_source "${ARGS[2]}" >> $FILE
echo "'" >> $FILE
echo "
declare -A GLOBAL
GLOBAL['NAME']='${ARGS[1]}'
GLOBAL['BUILD_SCRIPT']='$BUILD_SCRIPT_NAME'
GLOBAL['PACKAGE_VERSION']='$CRIPT_PACKAGE_VERSION'
" >> $FILE
get_marked_section "$BASH_SOURCE" "PACKAGE" >> $FILE || {
    echo "<!> An error was encountered (Bash is probably too old and doesn't define BASH_SOURCE)." 1>&2 
    exit 1
}
chmod +x $FILE

[[ "${ARGS[x]}" ]] || {
    if gpg --list-secret-keys "${ARGS[3]}" > /dev/null; then
        gpg --local-user "${ARGS[3]}" --detach-sign $FILE || {
            echo "<!> File "'`'"$FILE' was not signed!" 1>&2 
            exit 1
        }
    else
        echo -e '\n<W> Warning:
    Cript was not able to find a private key associated with "'"${ARGS[3]}"'".
    As a software author, you have a responsibility to protect your 
    users whom often do not have the knowledge to verify the integrity 
    of what they receive.
    
    By signing your package, you enable them to easily verify the 
    authenticity of the software in their position.
    
    It might be a minor annoyance to you, but it is a major advantage 
    to your users.\n'
    fi
}
echo 'CRIPT package `'$FILE"' has been generated successfully."

exit 0









#-------------------------------------------------------------------------------
#MARKER:PACKAGE:BEGIN
# Start of linux package logic

[[ "$CRIPT_CACHE_DIR" ]] || CRIPT_CACHE_DIR="/tmp"

hs() {
    cat $1 | sha1sum | sed s/\ .*$//
}

print() {
    [[ "${GLOBAL['VERBOSE']}" ]] && {
        echo "$1" > /dev/tty || echo "$(date): $1" >> "$CRIPT_CACHE_DIR/$USER.${GLOBAL['NAME']}.script.log"
    }
}

uncompress_source() {
    #stdin: base64/gz/tar string
    base64 -d | tar -xzf - || {
        return 1
    }
    return 0
}	

# if simple extraxt
[[ "$CRIPT_OP" == "EXTRACT" ]] && {
    echo -n "$0: Extracting.. "
    echo -n "$SOURCE_TARBALL" | tr -d '\n\t\ ' | uncompress_source
    echo "done"
    exit 0
}

[[ "$CRIPT_OP" == "INFO" ]] && {
    echo "
  cript package version ${GLOBAL['PACKAGE_VERSION']}
  
  Environment variables cript understands:
  
      CRIPT_VERBOSE='yes'             Print extra information to tty. If 
                                      script is not attached to a tty, it 
                                      is written to 
                                            
                                        /tmp/USER.{NAME}.script.log
                                        
                                      if CRIPT_CACHE_DIR is not set, or 
                                          
                                        CRIPT_CACHE_DIR/USER.{NAME}.script.log
                                      
      CRIPT_CACHE_DIR='{PATH}'        Set {PATH} as cache directory 
                                      (default is /tmp). This can be used 
                                      give executables a more resistant 
                                      home.
      
      CRIPT_OP='EXTRACT'              Extract compressed source into the 
                                      current working directory and stop.
      
      CRIPT_OP='INFO'                 Print information about the cript 
                                      package itself.
            "
    exit 0
}


GLOBAL['VERBOSE']="$CRIPT_VERBOSE"
GLOBAL['SCRIPT']="$BASH_SOURCE"

# setting up cache directory
GLOBAL['CACHE']="$CRIPT_CACHE_DIR/$USER.${GLOBAL['NAME']}.cache.d"

# if parent directories for the cache do not exist, mkdir them
[[ -d "$CRIPT_CACHE_DIR" ]] || mkdir -p "$CRIPT_CACHE_DIR"

# if the cache is stale, flush
# if script is newer than the cache
[[ -d "${GLOBAL['CACHE']}" ]] && ! [[ -x "${GLOBAL['CACHE']}/build/${GLOBAL['NAME']}" ]] && {
    print "<i> Flushing stale cache (no executable)"
    rm -rf "${GLOBAL['CACHE']}"
}

[[ -d "${GLOBAL['CACHE']}" ]] && [[ $(stat -c %Y "${GLOBAL['SCRIPT']}") -gt $(stat -c %Y "${GLOBAL['CACHE']}") ]] && {
    print "<i> Flushing stale cache (mtime variance)"
    rm -rf "${GLOBAL['CACHE']}"
}
# if hash mismatch
[[ -d "${GLOBAL['CACHE']}" ]] && 
    [[ $(hs "${GLOBAL['SCRIPT']}") != $([[ -e "${GLOBAL['CACHE']}/checksum" ]] && cat "${GLOBAL['CACHE']}/checksum") ]] && {
    print "<i> Flushing stale cache (checksum variance)"
    rm -rf "${GLOBAL['CACHE']}"
}



# if cache is not present or has been deleted by previous checks, rebuild
[[ -d "${GLOBAL['CACHE']}" ]] || {
    build_error() {
        [[ "$1" ]] && echo "$1" >&2
        rm -rf "${GLOBAL['CACHE']}"
        echo -e "\nAbort." >&2
        exit 1
    }

    print "<i> Building cache"
    
    # dir setup
    print "+--> Setting up directory structure"
    mkdir "${GLOBAL['CACHE']}"
    mkdir "${GLOBAL['CACHE']}/source"
    mkdir "${GLOBAL['CACHE']}/build"
    
    # generating checksum
    print "+--> Generating checksum"
    hs "${GLOBAL['SCRIPT']}" > "${GLOBAL['CACHE']}/checksum"
    
    # unpacking
    print "+--> Unpacking compressed source tree"
    
    cd "${GLOBAL['CACHE']}/source" > /dev/null
    echo -n "$SOURCE_TARBALL" | tr -d '\n\t\ ' | uncompress_source || {
        build_error "<!> Could not uncompress source tree."
    }
    cd - > /dev/null
    
    # building
    print "+--> Building source"
    cd "${GLOBAL['CACHE']}/source"
    # Run the build script
    [[ -e "${GLOBAL['BUILD_SCRIPT']}" ]] || build_error "<!> Build script '${GLOBAL['BUILD_SCRIPT']}' was not found."
    OUTPUT=$(bash "${GLOBAL['BUILD_SCRIPT']}" 2>&1 < /dev/null) || BUILD_ERROR_STATUS="yes"
    # if build script prints anything, treat it as a fatal error.
    [[ "$OUTPUT$BUILD_ERROR_STATUS" ]] && {
        echo "<!> build script produced output when it shouldn't:" 1>&2
        echo "$OUTPUT" 1>&2
        build_error
    }
    cd - > /dev/null
    
    # Check that their exists an executable with the same name as GLOBAL[NAME]
    [[ -x "${GLOBAL['CACHE']}/source/${GLOBAL['NAME']}" ]] || 
        build_error "<!> Could not locate binary."
    # symlnk it
    cd "${GLOBAL['CACHE']}/build"
    ln -s "${GLOBAL['CACHE']}/source/${GLOBAL['NAME']}" || build_error "<!> Could not symlnk binary."
    cd - > /dev/null
    print "+--> Done"
}

# Run the script
print "<!> Handing off control to the binary."
exec "${GLOBAL['CACHE']}/build/${GLOBAL['NAME']}" "$@"

# end of linux package logic
#MARKER:PACKAGE:END


