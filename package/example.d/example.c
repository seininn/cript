#include <stdio.h>

int main(int argc, char **argv){
    int i;
    puts("This is a C program that has been compiled on the fly.");
    puts("The binary of which has been cached so that subsequent "
         "script envocations would run imediatly without "
         "compiling the program first.");
    for (i = 1;i < argc; ++i) 
        printf("%d%s argument passed to me is `%s'.\n", i, 
                (i==1 ? "st" : (
                    i==2? "nd" : (
                        i==3? "rd" : "th" ))), argv[i]);
    puts("Return status is the number of arguments.");
    return argc-1;
}
